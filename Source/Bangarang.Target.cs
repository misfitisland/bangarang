// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class BangarangTarget : TargetRules
{
	public BangarangTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("Bangarang");

		if(Type == UnrealBuildTool.TargetType.Editor)
		{
			ExtraModuleNames.AddRange(new string[] {"BangarangEditor"});
		}
 
	}
}
