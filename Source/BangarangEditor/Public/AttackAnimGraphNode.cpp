// Peter Pan's got kids? He can't fly, fight or crow. If any of you says this here scug ain't Peter Pan...cross the line.

#include "Classes/AttackAnimGraphNode.h"

#include "EditorCategoryUtils.h"

#define LOCTEXT_NAMESPACE "AttackAnimGraphNode"

FLinearColor 
UAttackAnimGraphNode::GetNodeTitleColor() const
{
	return FLinearColor(0.f, 0.7f, 0.2f);
}

FText 
UAttackAnimGraphNode::GetTooltipText() const
{
	return LOCTEXT("NodeToolTip", "Plays Character Attack animations based on player or AI input\n"
								  "Character type must have appropriate CharacterName that matches the asset name in C++ class Eg: AHuman::CharacterName == 'Human' \n"
								  "Loaded from '/Game/Models/Characters/CHARACTERNAME/Anim' based on attack type and equiped weapon \n"
						          "Eg: A Human Player Hitting R1 W/ A Longsword will play /Game/Models/Characters/Human/Attack_Light_LongSword");
}

FText 
UAttackAnimGraphNode::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	return LOCTEXT("NodeTitle", "Attack Animation Player");
}

FText 
UAttackAnimGraphNode::GetMenuCategory() const
{
	return FEditorCategoryUtils::GetCommonCategory(FCommonEditorCategory::Animation);
}

#undef LOCTEXT_NAMESPACE