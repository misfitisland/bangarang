// Peter Pan's got kids? He can't fly, fight or crow. If any of you says this here scug ain't Peter Pan...cross the line.

#pragma once

#include "Core/Animation/AttackAnimNode.h"

#include "CoreMinimal.h"
#include "Editor/AnimGraph/Classes/AnimGraphNode_Base.h"

#include "AttackAnimGraphNode.generated.h"

UCLASS()
class BANGARANGEDITOR_API UAttackAnimGraphNode : public UAnimGraphNode_Base
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, Category=Settings)
	FAttackAnimNode Node;

	// UEdGraphNode interface
	virtual FLinearColor GetNodeTitleColor() const override;
	virtual FText GetTooltipText() const override;
	virtual FText GetNodeTitle(ENodeTitleType::Type TitleType) const override;
	virtual FText GetMenuCategory() const override;
	// End of UEdGraphNode interface
};