// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class BangarangEditor : ModuleRules
{
	public BangarangEditor(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "AnimGraph", "AnimGraphRuntime", "BlueprintGraph", "Bangarang" });
        PrivateDependencyModuleNames.AddRange(new string[] { "InputCore", "UnrealEd", "AnimGraph", "AnimGraphRuntime"});
    }
}
