// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "BangarangGameMode.h"
#include "Characters/BangarangCharacter.h"
#include "Core/BangarangPlayerState.h"

#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerState.h"


ABangarangGameMode::ABangarangGameMode()
{
	// Default is BallMer
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/AssetBPs/Characters/Player"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	PlayerStateClass = ABangarangPlayerState::StaticClass();
}
