#pragma once

#include "CoreMinimal.h"
#include "Core/ActionPool.h"
#include "GameFramework/Character.h"

#include "BangarangCharacter.generated.h"

/** Forwards 
*/
class UBangarangAnimInstance;
class UParticleSystemComponent;

/** CharacterState
 * A direct representation of the animation states
 * Add new animation states before invalid
 * as it TRUELY is invalid
 */
UENUM(BlueprintType)
enum class CharacterState : uint8
{
	CS_Movement UMETA(Displayname="Movement"),
	CS_Attack UMETA(DisplayName="Attack"),
	CS_ChainedAttack UMETA(DisplayName="Chained Attack"),
	CS_Invalid UMETA(DisplayName="Invalid")
};

UCLASS(BlueprintType, Blueprintable, abstract)
class BANGARANG_API ABangarangCharacter : public ACharacter
{
	GENERATED_BODY()

	// Let our Animations drive us if it wishes
	friend class UBangarangAnimInstance;

public:
				ABangarangCharacter();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float		BaseTurnRate;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float		BaseLookUpRate;
	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "1", UIMin = "1"))
	float		WalkingSpeed;
	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "1", UIMin = "1"))
	float		SprintSpeed;
	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "1", UIMin = "1"))
	float		SprintAcceleration;
	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "1", UIMin = "1"))
	float		DecelerationMultiplier;
	UPROPERTY(Category = "Pawn|Combat", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0", UIMin = "0"))
	float		DamageInvincibilityTime;
	UPROPERTY(Category = "Pawn|Combat", VisibleAnywhere, BlueprintReadOnly)
	bool		bHasDoneDamageThisSwing;

	/** Asset Prefix for this character type
	* Used to identify assets that belong to this class
	*/
	UPROPERTY(Category = "Pawn", VisibleAnywhere, NoClear)
	FString			AssetName;

	/** Next Player Action
	* !!IN EDITOR NOTE!! Set by cpp class by player or AI input
	*
	* Is the next action to be processed, has ZERO bearing on our current state
	* nullptr if no action, or action has been processed
	* Used by systems like Animation to enact attacking, blocking, etc
	*/
	UPROPERTY(Category = "Pawn", EditAnywhere, NoClear, BlueprintReadWrite)
	FAction			NextAction;
	UPROPERTY(Category = "Pawn", EditAnywhere, NoClear, BlueprintReadWrite)
	FAction			CurrentAction;

	/** What we doin? */
	UFUNCTION(BlueprintCallable, Category="Pawn")
	CharacterState			CurrentState() const;
	UFUNCTION(BlueprintCallable, Category="Pawn|Combat")
	bool					IsAttacking()	const;
	UFUNCTION(BlueprintCallable, Category="Pawn|Combat")
	bool					CanCauseDamage() const;
	UFUNCTION(BlueprintCallable, Category="Pawn|Combat")
	bool					CanBeDamaged() const;
	UFUNCTION(BlueprintCallable, Category="Pawn|Combat")
	bool					IsReactingToDamage() const;
	UFUNCTION(BlueprintCallable, Category="Pawn|Combat")
	void					OnJustHitCharacter(ABangarangCharacter* hit);

	UFUNCTION(BlueprintPure, Category="Pawn|Stats")
	virtual float			GetHP() PURE_VIRTUAL(ABangarangCharacter::GetHP, return 0.f;);
	UFUNCTION(BlueprintPure, Category="Pawn|Stats")
	virtual float			GetMana() PURE_VIRTUAL(ABangarangCharacter::GetMana, return 0.f;);
	UFUNCTION(BlueprintPure, Category="Pawn|Stats")
	virtual FString			GetCharacterName() PURE_VIRTUAL(ABangarangCharacter::GetMana, return TEXT("PURE_VIRTUAL"););
	UFUNCTION(BlueprintPure, Category="Pawn|Stats")
	virtual float			GetStamina() PURE_VIRTUAL(ABangarangCharacter::GetStamina, return 0.f;);

	/** Current speed calculated every tick */
	UFUNCTION(BlueprintCallable, Category="Utilities|Transformation")
	float			GetSpeed();

	/** Are we falling? */
	UFUNCTION(BlueprintCallable, Category="Utilities|Transformation")
	bool			IsInAir();

	/** Called for forwards/backward input */
	void			MoveForward(float Value);

	/** Called for side to side input */
	void			MoveRight(float Value);

	/** Sprint Stuff */
	void			StartSprint();
	void			StopSprint();
	bool			IsSprinting() const;

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void			TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void			LookUpAtRate(float Rate);

	/** Gets a previously loaded character attack animation
	 * @param WeaponCategory
	 * @param AttackType
	 * @param Direction default = Right (unused/future)
	 * @ret	  UAnimationAsset* AnimationSequence or nullptr if doesn't exist/not loaded
	 */
	class UAnimationAsset* GetAttackAnim(AttackType, WeaponCategory, Direction = Direction::D_Right);

	/** Gives some loaded default animation for use in errors and such
	 * @ret	  UAnimationAsset* AnimationSequence or nullptr if doesn't exist/not loaded
	 */
	virtual UAnimationAsset* GetDefaultAnim() {return nullptr;}

protected:
	UFUNCTION()
	virtual void			ChildTick(float deltaTime)  PURE_VIRTUAL(ABangarangCharacter::GetStamina, ;);
	virtual void			PostInitializeComponents() override;
	virtual void			EndPlay(const EEndPlayReason::Type) override;

	class ABangarangPlayerState* GetPlayerState();
	
	ActionPool			    mActionPool;
	bool					mReactingToDamage;
	double					mTimeOfHit;

	UPROPERTY()
	class UBGI*				mBGI;
	UPROPERTY()
	UBangarangAnimInstance* mAnimInstance;
	UPROPERTY()
	class UObjectLibrary*	mAnimLibrary; 

	TMap<FString, UAnimationAsset*>	mLoadedMiscAnims;
	TMap<AttackType, TMap<WeaponCategory, TArray<UAnimationAsset*> > > mLoadedAttackAnims;

	// Base stats used for initialization of AI and PlayerStates
	static const float sBaseHP;
	static const float sBaseMana;
	static const float sBaseStamina;

	UFUNCTION()
	virtual void	SetHP(float hp) PURE_VIRTUAL(ABangarangCharacter::SetHP, ;);
	UFUNCTION()
	virtual void	SetMana(float mana) PURE_VIRTUAL(ABangarangCharacter::SetMana, ;);
	UFUNCTION()
	virtual void	SetCharacterName(FString name) PURE_VIRTUAL(ABangarangCharacter::SetName, ;);
	UFUNCTION()
	virtual void	SetStamina(float sta) PURE_VIRTUAL(ABangarangCharacter::SetStamina, ;);
	UFUNCTION()
	virtual void	ReactToDamage(AActor* damaged, float damage, class AController* orderedBy, FVector hitLocation, class UPrimitiveComponent* hitComponent, FName boneName, FVector shotDir, const class UDamageType* dmgType, AActor* causedBy) PURE_VIRTUAL(ABangarangCharacter::ReactToDamage, ;);

	// Hit Effects
	UPROPERTY()
	UParticleSystemComponent*	mSmoke;
	UPROPERTY()
	UParticleSystemComponent*	mSparks;

private:
	virtual void	Tick(float deltaTime) final;
	void			UpdateStats();
	void			EvaluateCurrentFrame();
	void			SprintCalc();
	void			DoMovement(const FAction&);
	void			LoadAnims();
	void			LoadParticleSystems();

	/*UFUNCTION()
	virtual void	IncomingDamage(AActor* damaged, float damage, const UDamageType* damType, AController* orderedBy, AActor* causedBy) final;*/
	UFUNCTION()
	virtual void	IncomingPointDamage(AActor* damaged, float damage, class AController* orderedBy, FVector hitLocation, class UPrimitiveComponent* hitComponent, FName boneName, FVector shotDir, const class UDamageType* dmgType, AActor* causedBy);

	/** Make the actor "sprint" */
	bool						mIsSprinting;
	int							mCurrentState;
	/** Casted Player state if we have one, to avoid future casts */
	UPROPERTY()
	ABangarangPlayerState*		mBPS;
};

/** Variant Specializations **/
template<> struct TVariantTraits<CharacterState>
{
	static EVariantTypes GetType() { return EVariantTypes::Custom; }
};
