// Copyright 2017-2018 JC Ricks All Rights Reserved.

#include "Human.h"

#include "Core/Animation/BangarangAnimInstance.h"
#include "Props/Inventory/Weapons/Melee.h"

AHuman::AHuman()
	: mYMod(1.f)
{
	// Player should update before most everything else
	PrimaryActorTick.TickGroup = TG_PrePhysics;

	AssetName = "BallMer";

	// Equip LongSword just right off the bat #reported
	HeldWeapon = nullptr;
}

UAnimationAsset*
AHuman::GetDefaultAnim()
{
	static FString path = (GWorld->IsEditorWorld() ? 
						FString("/Game/Models/Characters/BallMer/Anim/BallMer_Anim_root_Run") : 
						FString("/Game/Models/Characters/BallMer/Anim/BallMer_Anim_root_Attack_Light_LongSword"));

	static UAnimSequence* def = Cast<UAnimSequence>(StaticLoadObject(UAnimSequence::StaticClass(), nullptr, TEXT(""), *path)); 

	return def;
}

void
AHuman::ChildTick(float deltaTime)
{
	if(mActionPool.CurrentFrame())
	{
		// Handle attacks this frame
		TArray<FAction*> attacks = mActionPool.CurrentFrame()->OfType(ActionType::AT_Attack);
		if(attacks.Num())
		{
			for(FAction* act : attacks)
			{
				ProcessAttack(act);
			}

			mActionPool.CurrentFrame()->Remove(attacks);
		}
	}
}

void
AHuman::ProcessAttack(FAction* atk)
{
	if(!mAnimInstance || !atk)
		return;

	// If our attack window isn't open, doesn't matter what we want
	if(!mAnimInstance->IsAttackInputWindowOpen())
		return;

	AttackType at			= atk->Data[0].GetValue<AttackType>();
	//WeaponCategory wc		= atk->Data[1].GetValue<WeaponCategory>();
	Direction dir			= atk->Data[2].GetValue<Direction>();
	CharacterState state	= CurrentState();
	bool setAction			= false;

	// Info about animation system's 
	FAction&  animCur	    = CurrentAction;
	bool actionType     	= (animCur.ActType == ActionType::AT_Attack || animCur.ActType == ActionType::AT_ChainedAtk);

	//UE_LOG(LogTemp, Log, TEXT("Human : Processing Atk : %s"), *Utility::ActionTypeString(atk->ActType));
	//UE_LOG(LogTemp, Log, TEXT("Human : AnimCurrentAction : %s"), *Utility::ActionTypeString(animCur.ActType));
	//UE_LOG(LogTemp, Log, TEXT("Human : CurState : %s"), *Utility::CharacterStateString(state));

	switch(at)
	{
		case AttackType::ATK_Light:
		case AttackType::ATK_Heavy:
		{
			if(dir == Direction::D_Right)
			{
				if(state == CharacterState::CS_Movement)
				{
					if(!actionType)
						setAction = true;
				}
				else if(state == CharacterState::CS_Attack)
				{
					if(actionType)
					{
						AttackType curTy = animCur.Data[0].GetValue<AttackType>();
						if(curTy != at)
							atk->ActType = ActionType::AT_ChainedAtk;
					}
					setAction = true;
				}
				else if(state == CharacterState::CS_ChainedAttack)
				{
					if(actionType)
					{
						AttackType curTy = animCur.Data[0].GetValue<AttackType>();
						if(curTy == at)
							atk->ActType = ActionType::AT_ChainedAtk;
					}
					setAction = true;
				}
			}
		}
		break;

		default:
			return;
	};

	if(setAction)
	{
		NextAction = *atk;

		//AttackType at		= NextAction.Data[0].GetValue<AttackType>();
		//WeaponCategory wc	= NextAction.Data[1].GetValue<WeaponCategory>();

		//FString msg = FString(Utility::ActionTypeString(NextAction.ActType)).AppendChar(L':')
		//				.Append(Utility::AttackTypeString(at)).AppendChar(L':')
		//				.Append(Utility::WeaponCategoryString(wc));

		//UE_LOG(LogTemp, Log, TEXT("Human : setting action"));
		//UE_LOG(LogTemp, Log, TEXT("action:type:category - %s"), *msg);
	}
}

void 
AHuman::AJump()
{
	mActionPool.AddAction(FAction(ActionType::AT_Jump));
}

void 
AHuman::ASprintStart()
{
	mActionPool.AddAction(FAction(ActionType::AT_Sprint, {true}));
}

void 
AHuman::ASprintStop()
{
	mActionPool.AddAction(FAction(ActionType::AT_Sprint, {false}));
}

void 
AHuman::AMoveForward(float val)
{
	mActionPool.AddAction(FAction(ActionType::AT_Move,{MovementType::MT_MoveForwardV, val}));
}

void 
AHuman::AMoveRight(float val)
{
	mActionPool.AddAction(FAction(ActionType::AT_Move,{MovementType::MT_MoveLateralV, val}));
}

void
AHuman::Pitch(float val)
{
	val *= mYMod;

	mActionPool.AddAction(FAction(ActionType::AT_Move,{MovementType::MT_Pitch, val}));
}

void
AHuman::PitchAt(float val)
{
	val *= mYMod;

	mActionPool.AddAction(FAction(ActionType::AT_Move,{MovementType::MT_PitchRate, val}));
}

void
AHuman::Yaw(float val)
{
	mActionPool.AddAction(FAction(ActionType::AT_Move,{MovementType::MT_Yaw, val}));
}

void
AHuman::YawAt(float val)
{
	mActionPool.AddAction(FAction(ActionType::AT_Move,{MovementType::MT_YawRate, val}));
}

void
AHuman::OnRightLightAttack()
{
	if(HeldWeapon)
		mActionPool.AddAction(FAction(ActionType::AT_Attack,{AttackType::ATK_Light, GetHeldWeapon()->WeaponCategory, Direction::D_Right}));
}

void
AHuman::OnRightHeavyAttack()
{
	if(HeldWeapon)
		mActionPool.AddAction(FAction(ActionType::AT_Attack,{AttackType::ATK_Heavy, GetHeldWeapon()->WeaponCategory, Direction::D_Right}));
}

AWeapon*
AHuman::GetHeldWeapon() const 
{
	if(HeldWeapon)
		return HeldWeapon->GetDefaultObject<AWeapon>();

	return nullptr;
}