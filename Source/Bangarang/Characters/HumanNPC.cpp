// Peter Pan's got kids? He can't fly, fight or crow. If any of you says this here scug ain't Peter Pan...cross the line.

#include "HumanNPC.h"

#include "Core/BGI.h"

#include "Runtime/Engine/Classes/GameFramework/DamageType.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerState.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystem.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"

AHumanNPC::AHumanNPC()
	: AHuman(), mHP(sBaseHP), mMana(sBaseMana), mStamina(sBaseStamina), mName("SWOLEjer420")
{
}

void
AHumanNPC::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void
AHumanNPC::ReactToDamage(AActor* damaged, float damage, class AController* orderedBy, 
						 FVector hitLocation, class UPrimitiveComponent* hitComponent, 
						 FName boneName, FVector shotDir, const class UDamageType* dmgType, 
						 AActor* causedBy)
{		
}