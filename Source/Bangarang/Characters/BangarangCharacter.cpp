#include "BangarangCharacter.h"
#include "Core/BGI.h"
#include "Core/Animation/BangarangAnimInstance.h"
#include "Props/Inventory/Weapons/Weapon.h"

#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Core/BangarangPlayerState.h"
#include "Engine/ObjectLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystem.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"

#include "DrawDebugHelpers.h"


//////////////////////////////////////////////////////////////////////////
// ABangarangCharacter

const float ABangarangCharacter::sBaseHP(100.f);
const float ABangarangCharacter::sBaseMana(100.f);
const float ABangarangCharacter::sBaseStamina(100.f);

ABangarangCharacter::ABangarangCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate	= 45.f;
	BaseLookUpRate	= 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw	= false;
	bUseControllerRotationRoll	= false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement	= true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate				= FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity				= 600.f;
	GetCharacterMovement()->AirControl					= 0.2f;

	// If it's so heavy duty it needs a BCharacter, then it should tick.
	// Any outliers can disable if needed.
	PrimaryActorTick.bCanEverTick			= true;
	PrimaryActorTick.bStartWithTickEnabled	= true;
	PrimaryActorTick.bTickEvenWhenPaused	= true;
	PrimaryActorTick.TickGroup				= TG_PostPhysics;

	// WalkingSpeed = Fastest speed we can still be walking at
	// MaxWalkSpeed = Our Current Modified Max Speed
	WalkingSpeed							= 250.0f;
	GetCharacterMovement()->MaxWalkSpeed	= WalkingSpeed;

	// Sprint stuff
	SprintSpeed				= 500.0f;
	SprintAcceleration		= 1.5f;
	DecelerationMultiplier	= 2.0f;
	mIsSprinting			= false;

	// Action stuff
	NextAction			= FAction(ActionType::AT_Invalid);
	// Alerts children using actions that NextAction is open to be set
	NextAction.IsFresh	= false;

	// Anim
	mAnimInstance	= nullptr;
	mBGI			= nullptr;
	mAnimLibrary	= nullptr;

	// Default Name
	AssetName		= "ERROR-ABSTRACT";

	// Combat
	DamageInvincibilityTime	= 0.5f;
	bCanBeDamaged			= true;
	mReactingToDamage		= true;
	mTimeOfHit				= 0.0;

	// Misc
	mBPS = nullptr;
	mSparks = nullptr;
	mSmoke = nullptr;

	//FScriptDelegate genDmg;
	//genDmg.BindUFunction(this, "IncomingDamage");
	//OnTakeAnyDamage.Add(genDmg);

	FScriptDelegate pointDmg;
	pointDmg.BindUFunction(this, "IncomingPointDamage");
	OnTakePointDamage.Add(pointDmg);
	
	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

void
ABangarangCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	UWorld* world = GetWorld();
	if(!world)
		return;

	if(world->IsEditorWorld() && !world->IsPlayInEditor())
		return;

	// Cast Werk
	mBGI = Cast<UBGI>(GetGameInstance());
	mBPS = Cast<ABangarangPlayerState>(PlayerState);

	// Get any assets we're using on the fly
	LoadAnims();
	LoadParticleSystems();
}

void
ABangarangCharacter::EndPlay(const EEndPlayReason::Type reason)
{
	Super::EndPlay(reason);

	if(mSmoke)
	{
		UParticleSystemComponent* smokeOut =
			UGameplayStatics::SpawnEmitterAtLocation(GWorld, mSmoke->Template, GetActorLocation());

		smokeOut->SetWorldScale3D(FVector(10.f,10.f,10.f));
	}

	if(mSparks)
	{
		UParticleSystemComponent* sparkOut =
			UGameplayStatics::SpawnEmitterAtLocation(GWorld, mSparks->Template, GetActorLocation());
		sparkOut->SetWorldScale3D(FVector(10.f,10.f,10.f));
	}
}

ABangarangPlayerState*
ABangarangCharacter::GetPlayerState()
{
	return mBPS;
}

//void	
//ABangarangCharacter::IncomingDamage(AActor* damaged, float damage, const UDamageType* damType, AController* orderedBy, AActor* causedBy)
//{
//	GLog->Log("General Damage");
//
//	SetHP(GetHP() - damage);
//
//	mReactingToDamage	= true;
//	bCanBeDamaged		= false;
//	mTimeOfHit			= FApp::GetCurrentTime();
//
//	UE_LOG(LogTemp, Log, TEXT("***%s***"), *GetCharacterName());
//	UE_LOG(LogTemp, Log, TEXT("%s Dmg : recieved"), *FString::SanitizeFloat(damage));
//	UE_LOG(LogTemp, Log, TEXT("%s HP"), *FString::SanitizeFloat(GetHP()));
//
//	int wut = damev.ClassID;
//	FString lol;
//	lol.AppendInt(wut);
//	UE_LOG(LogTemp, Error, TEXT("FUCK! FUCK! %s"), *lol);
//
//	if(GetHP() <= 0.f)
//	{
//		Destroy();
//		UE_LOG(LogTemp, Log, TEXT("GOODBYE CRUEL WORLD!!!!"));
//	}
//
//	// Children can opt to do something if they wish
//	ReactToDamage(damaged, damage, damev, orderedBy, causedBy);
//}

void	
ABangarangCharacter::IncomingPointDamage(AActor* damaged, float damage, class AController* orderedBy, FVector hitLocation, class UPrimitiveComponent* hitComponent, FName boneName, FVector shotDir, const class UDamageType* dmgType, AActor* causedBy)
{
	// False positive, at least for what I care about
	if(boneName.IsNone())
		return;

	mReactingToDamage	= true;
	bCanBeDamaged		= false;
	mTimeOfHit			= FApp::GetCurrentTime();

	SetHP(GetHP() - damage);

	FVector weaponOrigin;
	FVector exts;
	FVector rightVec = causedBy->GetActorRightVector();
	causedBy->GetActorBounds(true, weaponOrigin, exts);

	FVector backVec = hitLocation + (shotDir * exts);

	if(mBGI->GlobalDebug)
		DrawDebugLine(GWorld, hitLocation, backVec, FColor::Red, true);

	if(GetHP() <= 0.f)
	{
		Destroy();
		UE_LOG(LogTemp, Log, TEXT("GOODBYE CRUEL WORLD!!!!"));
	}

	// Do Hit Particles
	if(mSmoke && mSparks)
	{
		if(mSmoke->IsActive())
		{
			mSmoke->DeactivateSystem();
		}

		if(mSparks->IsActive())
		{
			mSparks->DeactivateSystem();
		}

		if(ABangarangCharacter* bc = Cast<ABangarangCharacter>(orderedBy->GetPawn()))
		{
			bc->OnJustHitCharacter(this);
		}

		mSmoke->AttachToComponent(hitComponent, FAttachmentTransformRules::KeepWorldTransform, boneName);
		mSparks->AttachToComponent(hitComponent, FAttachmentTransformRules::KeepWorldTransform, boneName);

		mSmoke->SetWorldLocation(hitLocation);
		mSparks->SetWorldLocation(hitLocation);

		mSmoke->ActivateSystem();
		mSparks->ActivateSystem();
	}

	// Children can opt to do something if they wish
	ReactToDamage(damaged, damage, orderedBy, hitLocation, hitComponent, boneName, shotDir, dmgType, causedBy);
}

float
ABangarangCharacter::GetSpeed()
{
		// FVector "Size" == Magnitude/Len lol wtf
	return (GetVelocity().Size());
}

bool
ABangarangCharacter::IsInAir()
{
	return GetCharacterMovement()->IsFalling();
}

CharacterState
ABangarangCharacter::CurrentState() const
{
	return static_cast<CharacterState>(mCurrentState);
}

bool
ABangarangCharacter::CanCauseDamage() const
{
	if(!mAnimInstance)
		return false;

	return (IsAttacking() && mAnimInstance->IsAttackInputWindowOpen() && !bHasDoneDamageThisSwing);
}

bool
ABangarangCharacter::CanBeDamaged() const
{
	return bCanBeDamaged;
}

bool
ABangarangCharacter::IsReactingToDamage() const
{
	return mReactingToDamage;
}

void
ABangarangCharacter::OnJustHitCharacter(ABangarangCharacter* hit)
{
	bHasDoneDamageThisSwing = true;
}

bool
ABangarangCharacter::IsAttacking() const
{
	return (CurrentState() == CharacterState::CS_Attack || CurrentState() == CharacterState::CS_ChainedAttack);
}

void 
ABangarangCharacter::Tick(float deltaTime)
{
	Super::Tick(deltaTime);

	// Update any relevant stats used by blueprints and possibly child classes (eg: health etc)
	UpdateStats();

	// Update child stuff first
	ChildTick(deltaTime);

	// Evaluate any actions left over from child class (usually movement)
	EvaluateCurrentFrame();

	// Sprint speed calculations and reaction
	SprintCalc();
}

void
ABangarangCharacter::UpdateStats()
{
	if(!mAnimInstance)
		mAnimInstance = Cast<UBangarangAnimInstance>(GetMesh()->AnimScriptInstance);

	if(IsReactingToDamage() && !CanBeDamaged())
	{
		// Took damage and need to be invincible for like a sec, yah know?
		// TODO: Maybe replace this with a "not timer" that doesn't work on magic numbers
		double delta	= FApp::GetCurrentTime() - mTimeOfHit;

		if(delta >= static_cast<double>(DamageInvincibilityTime))
		{
			bCanBeDamaged		= true;
			mReactingToDamage	= false;
			mTimeOfHit			= 0.0;
		}
	}
}

void
ABangarangCharacter::EvaluateCurrentFrame()
{
	if(ActionFrame* frame = mActionPool.CurrentFrame())
	{
		TArray<FAction>& acts = frame->Actions();

		while(acts.Num())
		{
			FAction act = acts.Pop();
			switch(act.ActType)
			{
			case ActionType::AT_Move:
			{
				DoMovement(act);
			}
			break;

			case ActionType::AT_Jump:
			{
				if(IsSprinting())
					Jump();
			}
			break;

			case ActionType::AT_Sprint:
			{
				if(CurrentState() == CharacterState::CS_Movement &&
				   act.Data[0].GetValue<bool>())
				{
					StartSprint();						
				}
				else
					StopSprint();
			}
			break;

			default:
				break;
			}
		}
	}

	// Action Pool should be notified
	mActionPool.CurrentFrameComplete();
}

void
ABangarangCharacter::SprintCalc()
{
	float curMax = GetCharacterMovement()->MaxWalkSpeed;

	if (mIsSprinting)
	{
		if(curMax < SprintSpeed)
			GetCharacterMovement()->MaxWalkSpeed += SprintAcceleration;
	}
	else if (curMax > WalkingSpeed)
	{
		curMax -= (SprintAcceleration * DecelerationMultiplier);

		if (curMax < WalkingSpeed)
			curMax = WalkingSpeed;

		GetCharacterMovement()->MaxWalkSpeed = curMax;
	}
}

void
ABangarangCharacter::DoMovement(const FAction& act)
{
	MovementType subType = act.Data[0].GetValue<MovementType>();
	float value			 = act.Data[1].GetValue<float>();
	bool moveState		 = (CurrentState() == CharacterState::CS_Movement);

	switch(subType)
	{
		case MovementType::MT_MoveForwardV:
		{
			if(moveState)
				MoveForward(value);
		}
		return;

		case MovementType::MT_MoveLateralV:
		{
			if(moveState)
				MoveRight(value);
		}
		return;

		case MovementType::MT_Pitch:
		{
			AddControllerPitchInput(value);
		}
		return;

		case MovementType::MT_PitchRate:
		{
			LookUpAtRate(value);
		}
		return;

		case MovementType::MT_Yaw:
		{
			AddControllerYawInput(value);
		}
		return;

		case MovementType::MT_YawRate:
		{
			TurnAtRate(value);
		}
		return;

		default:
			return;
	}
}

UAnimationAsset*
ABangarangCharacter::GetAttackAnim(AttackType type, WeaponCategory category, Direction)
{
	FString msg = FString(Utility::AttackTypeString(type)).AppendChar(L':').Append(Utility::WeaponCategoryString(category));

	// *NOTE* Direction is currently unused.
	// TODO:JC Figure out some way to animate right side w/o re doing animations (LMAO, FUCK ME)
	UAnimationAsset* ret = mLoadedAttackAnims[type][category].Top();
	return ret;
}

void
ABangarangCharacter::LoadAnims()
{
	if(mBGI)
	{
		// Prep Attack anim data structure
		mLoadedAttackAnims.Add(AttackType::ATK_Light);
		mLoadedAttackAnims.Add(AttackType::ATK_Heavy);

		static FString atkStr("Attack_");
		static FString rootStr("root_");

		// Search strings, if found are assets we need, strip them load in appropriate areas
		FString likely = FString("%c_Anim_").Replace(TEXT("%c"), *AssetName);
		FString unlikely = FString("%cAnims_").Replace(TEXT("%c"), *AssetName);

		TArray<UAnimationAsset*> anims = mBGI->AHelper().GetCharacterAnimations(AssetName, true);
		for(UAnimationAsset* anim : anims)
		{
			FString shortName = anim->GetName();
			if(shortName.RemoveFromStart(likely) || shortName.RemoveFromStart(unlikely))
			{
				// Sometimes _root_ is appended
				shortName.RemoveFromStart(rootStr);

				if(shortName.RemoveFromStart(atkStr))
				{
					int index;
					shortName.FindChar(L'_', index);

					//UE_LOG(LogLoad, Log, TEXT("shortName: %s"), *shortName);
					//UE_LOG(LogLoad, Log, TEXT("shortName.Left: %s"), *shortName.Left(index));

					AttackType ty = Utility::StringToAttackType(shortName.Left(index));
					if(ty == AttackType::ATK_Invalid)
					{
						UE_LOG(LogLoad, Error, TEXT("Invalid Attack Type, Anim Skipped"));
						continue;
					}

					// Chop past the _ to get WeaponCat name
					shortName = shortName.RightChop(index + 1);

					//UE_LOG(LogLoad, Log, TEXT("shortName: %s"), *shortName);

					WeaponCategory wc = Utility::StringToWeaponCategory(shortName);
					if(wc == WeaponCategory::WC_Invalid)
					{
						UE_LOG(LogLoad, Error, TEXT("Invalid Weapon Category, Anim Skipped"));
						continue;
					}

					if(!mLoadedAttackAnims[ty].Contains(wc))
						mLoadedAttackAnims[ty].Emplace(wc);
							
					/*******************
					* Eye catching Block
					* For main ADD Operation
					*/
					mLoadedAttackAnims[ty][wc].Add(anim);

					UE_LOG(LogLoad, Verbose, TEXT("Added Attack Anim"));
				}
				else
				{
					mLoadedMiscAnims.Emplace(shortName, anim);

					UE_LOG(LogLoad, Verbose, TEXT("Added Misc Anim"));
				}
			}
			else
			{
				UE_LOG(LogLoad, Warning, TEXT("Asset Not AnimSequence, Skipped"));
			}
		}			
	}
}

void
ABangarangCharacter::LoadParticleSystems()
{
	FString error;

	// For our hit we need sparks and smoke as we are totes a human, right?
	UParticleSystem* smoke	= mBGI->AHelper().GetParticleSystem(EffectType::ET_SmokeHit, 0, true);
	UParticleSystem* sparks = mBGI->AHelper().GetParticleSystem(EffectType::ET_SparkHit, 0, true);
	if(smoke && sparks) // I'm JC and this has been my favorite If Check on The Citadel
	{
		mSmoke = NewObject<UParticleSystemComponent>(this, TEXT("SmokeHit"));
		if(mSmoke)
		{
			mSmoke->bAutoActivate = false;
			mSmoke->SetTemplate(smoke);
			mSmoke->RegisterComponent();
		}
		else
			error.Append(TEXT("Smoke "));

		mSparks = NewObject<UParticleSystemComponent>(this, TEXT("SparksHit"));
		if(mSparks)
		{
			mSparks->bAutoActivate = false;
			mSparks->SetTemplate(sparks);
			mSparks->RegisterComponent();
		}
		else
			error.Append(TEXT("Sparks "));

		if(!error.IsEmpty())
			UE_LOG(LogLoad, Error, TEXT("Particle System Comp : Failed To Load : %s"), *error);

	}
	else
		UE_LOG(LogLoad, Error, TEXT("Failed to load smoke and/or sparks particle systems in an actor."));
}

void ABangarangCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ABangarangCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ABangarangCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		/*FString vel = FString("Velocity: ").Append(GetVelocity().ToString());
		UE_LOG(LogTemp, Log, TEXT("%s"), *vel);*/

		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		//FString lo = FString("Value:").Append(FString::SanitizeFloat(Value));
		//UE_LOG(LogTemp, Log, TEXT("%s"), *lo);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ABangarangCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void ABangarangCharacter::StartSprint()
{
	mIsSprinting = true;
}

void ABangarangCharacter::StopSprint()
{
	mIsSprinting = false;
}

bool ABangarangCharacter::IsSprinting() const
{
	return mIsSprinting;
}