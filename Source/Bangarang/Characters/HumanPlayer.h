// Peter Pan's got kids? He can't fly, fight or crow. If any of you says this here scug ain't Peter Pan...cross the line.

#pragma once

#include "CoreMinimal.h"
#include "Characters/Human.h"
#include "HumanPlayer.generated.h"

//TODO:JC Replace this class with Player AController component or something like that

/**
 * Handles player input for a character with human characteristics
 */
UCLASS()
class BANGARANG_API AHumanPlayer : public AHuman
{
	GENERATED_BODY()

public:
					AHumanPlayer();

	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	virtual float	GetHP() final;
	virtual float	GetMana() final;
	virtual FString	GetCharacterName() final;
	virtual float	GetStamina() final;
	
protected:
	// APawn interface
	virtual void	PostInitializeComponents() final;
	virtual void	SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) final;
	// End of APawn interface

private:
	// Camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent*	CameraBoom;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent*	FollowCamera;

	virtual void	ReactToDamage(AActor* damaged, float damage, class AController* orderedBy, FVector hitLocation, class UPrimitiveComponent* hitComponent, FName boneName, FVector shotDir, const class UDamageType* dmgType, AActor* causedBy) final;

	virtual void	SetHP(float hp) final;
	virtual void	SetMana(float mana) final;
	virtual void	SetCharacterName(FString name) final;
	virtual void	SetStamina(float sta) final;

	void			DebugToggle();
	void			AIDebugToggle();
	void			SpawnEnemy();
    void            Quit();
    void            ForceQuit();
};
