// Peter Pan's got kids? He can't fly, fight or crow. If any of you says this here scug ain't Peter Pan...cross the line.

#pragma once

#include "CoreMinimal.h"
#include "Characters/Human.h"
#include "HumanNPC.generated.h"

//TODO:JC Replace this class with AI AController component or something like that

/**
 * Handles AI input for a character with human characteristics
 */
UCLASS()
class BANGARANG_API AHumanNPC : public AHuman
{
	GENERATED_BODY()
	
public:
	AHumanNPC();

	virtual float	GetHP() final;
	virtual float	GetMana() final;
	virtual FString	GetCharacterName() final;
	virtual float	GetStamina() final;

	// TODO: AI stuff when I figure out what that is
protected:
	// APawn interface
	virtual void	PostInitializeComponents() final;

private:
	virtual void	ReactToDamage(AActor* damaged, float damage, class AController* orderedBy, FVector hitLocation, class UPrimitiveComponent* hitComponent, FName boneName, FVector shotDir, const class UDamageType* dmgType, AActor* causedBy) final;

	virtual void	SetHP(float hp) final;
	virtual void	SetMana(float mana) final;
	virtual void	SetCharacterName(FString name) final;
	virtual void	SetStamina(float sta) final;

	// NPC STATS
	float	mHP;
	float	mMana;
	float	mStamina;
	FString mName;
};

inline float	AHumanNPC::GetHP() {return mHP;}
inline float	AHumanNPC::GetMana() {return mMana;}
inline FString	AHumanNPC::GetCharacterName() {return mName;}
inline float	AHumanNPC::GetStamina() {return mStamina;}

inline void AHumanNPC::SetHP(float hp) {mHP = hp;}
inline void AHumanNPC::SetMana(float mana) {mMana = mana;}
inline void AHumanNPC::SetCharacterName(FString name) {mName = name;}
inline void AHumanNPC::SetStamina(float sta) {mStamina = sta;}