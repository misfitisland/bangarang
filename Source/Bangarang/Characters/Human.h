// Copyright 2017-2018 JC Ricks All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "BangarangCharacter.h"
#include "Human.generated.h"

/**
 * 
 */
UCLASS(config=Player, abstract)
class BANGARANG_API AHuman : public ABangarangCharacter
{
	GENERATED_BODY	()

public:
					AHuman		();
	virtual			~AHuman		() = default;

	/** Held weapon
	*   spawned in world
	*/
	UPROPERTY()
	TSubclassOf<AWeapon>	HeldWeapon;

	virtual UAnimationAsset* GetDefaultAnim() final;

	// Movement input
	void AJump();
	void ASprintStart();
	void ASprintStop();
	void AMoveForward(float);
	void AMoveRight(float);

	void Pitch(float);
	void PitchAt(float);
	void Yaw(float);
	void YawAt(float);

	// Attacks / Actions
	void OnRightLightAttack();
	void OnRightHeavyAttack();

protected:
	// Y sensitivity (I put it negative because I'm of superior genetics and play inverted)
	float mYMod;

	// Convienience call for templated weapon class container
	AWeapon* GetHeldWeapon() const;

private:
	virtual void	ChildTick	(float) final;
	// Utility
	void ProcessAttack(FAction*);
};
