// Peter Pan's got kids? He can't fly, fight or crow. If any of you says this here scug ain't Peter Pan...cross the line.

#include "HumanPlayer.h"

#include "HumanNPC.h"
#include "Core/BGI.h"
#include "Props/Inventory/Weapons/Melee.h"

#include "Camera/CameraComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Core/BangarangPlayerState.h"
#include "Engine/LocalPlayer.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/GameState.h"

AHumanPlayer::AHumanPlayer() 
	: AHuman()
{
	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// inverted is the superior method btw
	mYMod = -1.f;
}

void
AHumanPlayer::PostInitializeComponents()
{	
	Super::PostInitializeComponents();

	UWorld* world = GetWorld();
	if(!world)
		return;

	if(world->IsEditorWorld() && !world->IsPlayInEditor())
		return;

	if(GetPlayerState() && GetPlayerState()->bInitNeeded)
	{
		ABangarangPlayerState* is = GetPlayerState();
		is->SetPlayerName(TEXT("Penis McFadden"));
		is->HP			= sBaseHP;
		is->Mana		= sBaseMana;
		is->Stamina		= sBaseStamina;

		is->bInitNeeded = false;
	}
	
	if(TSubclassOf<AActor> wep = mBGI->AHelper().GetWeapon("TestWep", true))
	{
		HeldWeapon = wep;

		if(HeldWeapon)
		{
			FActorSpawnParameters params;
			params.Owner = this;
			params.Instigator = this;

			AWeapon* spawned = world->SpawnActor<AWeapon>(HeldWeapon, params);
			spawned->AttachToComponent(Cast<USceneComponent>(this->GetMesh()), FAttachmentTransformRules::SnapToTargetNotIncludingScale, TEXT("hand_right_weapon"));
		}
		else
		{
			UE_LOG(LogLoad, Error, TEXT("Wep was sublcass of AActor but failed to assign to HeldWeapon"));
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void 
AHumanPlayer::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);

	// Grab our player if we're being controlled by one
	// I mean we're in player input soooooooooo


	/** ACTIONS */
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AHuman::AJump);
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &AHuman::ASprintStart);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &AHuman::ASprintStop);

	// Attacks
	PlayerInputComponent->BindAction("RightLightAttack", IE_Pressed, this, &AHuman::OnRightLightAttack);
	PlayerInputComponent->BindAction("RightHeavyAttack", IE_Pressed, this, &AHuman::OnRightHeavyAttack);

	// Debug / Testing
	PlayerInputComponent->BindAction("DebugToggle", IE_Pressed, this, &AHumanPlayer::DebugToggle);
	PlayerInputComponent->BindAction("AIDebugToggle", IE_Pressed, this, &AHumanPlayer::AIDebugToggle);
	PlayerInputComponent->BindAction("SpawnEnemy", IE_Pressed, this, &AHumanPlayer::SpawnEnemy);
    PlayerInputComponent->BindAction("Quit", IE_Pressed, this, &AHumanPlayer::Quit);
    PlayerInputComponent->BindAction("ForceQuit", IE_Pressed, this, &AHumanPlayer::ForceQuit);

	/** AXIS */
	PlayerInputComponent->BindAxis("MoveForward", this, &AHuman::AMoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AHuman::AMoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &AHuman::Yaw);
	PlayerInputComponent->BindAxis("TurnRate", this, &AHuman::YawAt);
	PlayerInputComponent->BindAxis("LookUp", this, &AHuman::Pitch);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AHuman::PitchAt);
}

void
AHumanPlayer::ReactToDamage(AActor* damaged, float damage, class AController* orderedBy, 
							FVector hitLocation, class UPrimitiveComponent* hitComponent, 
							FName boneName, FVector shotDir, const class UDamageType* dmgType, 
							AActor* causedBy)
{
}

void
AHumanPlayer::DebugToggle()
{
	mBGI->GlobalDebug = !mBGI->GlobalDebug;
}

void
AHumanPlayer::AIDebugToggle()
{
	mBGI->AIDebug = !mBGI->AIDebug;
}

void
AHumanPlayer::SpawnEnemy()
{
	// Try on Forward Vector, if fails, do right vector, after that fuck off
	FVector spawnPos = GetActorLocation();
	spawnPos += GetActorForwardVector() * 300.f;

	TSubclassOf<AActor> toSpawn = mBGI->AHelper().GetCharacter("AIHuman", true);

	FTransform trans = FTransform(spawnPos);

	if(!toSpawn)
		return;

	UE_LOG(LogTemp, Log, TEXT("Spawning..."));

	if(AActor* spawned = GetWorld()->SpawnActor(toSpawn, &trans))
	{
		if(APawn* pa = Cast<APawn>(spawned))
			GetWorld()->AddPawn(pa);

		return;
	}

	UE_LOG(LogTemp, Log, TEXT("Forward failed, spawning on right..."));
	spawnPos = GetActorLocation();
	spawnPos += GetActorRightVector() * 300.f;

	trans = FTransform(spawnPos);

	if(GetWorld()->SpawnActor(toSpawn, &trans))
		UE_LOG(LogTemp, Log, TEXT("Spawned on right!"));
}

void
AHumanPlayer::Quit()
{
    mBGI->RequestGameExit(false);
}

void
AHumanPlayer::ForceQuit()
{
    mBGI->RequestGameExit(true);
}

float	AHumanPlayer::GetHP() {return GetPlayerState()->HP;}
float	AHumanPlayer::GetMana() {return GetPlayerState()->Mana;}
FString AHumanPlayer::GetCharacterName() {return GetPlayerState()->GetPlayerName();}
float	AHumanPlayer::GetStamina() {return GetPlayerState()->Stamina;}

void AHumanPlayer::SetHP(float hp) {GetPlayerState()->HP = hp;}
void AHumanPlayer::SetMana(float mana) {GetPlayerState()->Mana = mana;}
void AHumanPlayer::SetCharacterName(FString name) {GetPlayerState()->SetPlayerName(name);}
void AHumanPlayer::SetStamina(float sta) {GetPlayerState()->Stamina = sta;}
