// Peter Pan's got kids? He can't fly, fight or crow. If any of you says this here scug ain't Peter Pan...cross the line.

#include "BGI.h"

#include "MJAssetLibrary.h"

#include "Runtime/Engine/Classes/Animation/AnimSequence.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystem.h"

UBGI::UBGI()
	: UGameInstance(), GlobalDebug(false), AIDebug(false)
{
	mAL				= NewObject<UMJAssetLibrary>();
	mAssetHelper.SetLib(mAL);
}

void
UBGI::RequestGameExit(bool force)
{
    // dangerous!
    if(force)
        abort();
    else
        GIsRequestingExit = true;
}

AssetHelper::AssetHelper()
	: mLib(nullptr)
{
}

TSubclassOf<AActor> 
AssetHelper::GetCharacter(const FString& name, bool tryLoad)
{
	static FString charDir("/Game/AssetBPs/Characters/");
	
	if(!name.IsEmpty())
	{
		FString ndotn(name);
		ndotn.Append(".").Append(name);

		TArray<TSubclassOf<AActor> > found = mLib->GetActorsAtPath(FString(charDir).Append(ndotn), tryLoad);
		if(found.Num() == 1)
			return found.Top();
	}

	return nullptr;
}

TArray<UAnimationAsset*>
AssetHelper::GetCharacterAnimations(const FString& name, bool tryLoad)
{
	static FString modelsDir("/Game/RawAssets/Models/Characters/");
	static FString animDir("/Anim");

	UObjectLibrary* result	= nullptr;
	FString path			= FString(modelsDir).Append(name).Append(animDir);
	FString debug			= FString("Getting Character Anims: ").Append(path);
	UE_LOG(LogLoad, Log, TEXT("%s"), *debug);

	TArray<UAnimationAsset*> ret;

	TArray<UObject*> objs = mLib->GetObjectsAtPathByType(path, UAnimationAsset::StaticClass(), true);
	UE_LOG(LogLoad, Log, TEXT("num objs: %s"), *FString::FromInt(objs.Num()));

	for(UObject* obj : objs)
	{
		if(UAnimationAsset* ass = Cast<UAnimationAsset>(obj))
		{
			ret.Add(ass);
		}
	}

	UE_LOG(LogLoad, Log, TEXT("Returning % animations"), *FString::FromInt(ret.Num()));
	return ret;
}

TSubclassOf<AActor>
AssetHelper::GetWeapon(const FString& name, bool tryLoad)
{
	static FString weapDir("/Game/AssetBPs/Props/Inventory/Weapons/");

	if(!name.IsEmpty())
	{
		FString ndotn(name);
		ndotn.Append(".").Append(name);

		TArray<TSubclassOf<AActor> > found = mLib->GetActorsAtPath(FString(weapDir).Append(ndotn), tryLoad);
		if(found.Num() == 1)
			return found.Top();
	}

	return nullptr;
}

UParticleSystem*
AssetHelper::GetParticleSystem(EffectType type, int num, bool tryLoad)
{
	static FString partDir("/Game/Particles/Systems");
	
	if(type == EffectType::ET_Invalid)
		return nullptr;

	if(tryLoad)
	{
		// Load up all our systems for reference
		TArray<UObject*> objs = mLib->GetObjectsAtPath(partDir, true);
		for(UObject* obj : objs)
		{
			if(UParticleSystem* system = Cast<UParticleSystem>(obj))
			{
				EffectType key = Utility::StringToEffectType(obj->GetName());
				if(key != EffectType::ET_Invalid)
				{
					if(!mParticleSystems.Contains(key))
						mParticleSystems.Add(key);
					else if(mParticleSystems[key].Contains(system))
						continue;
				
					mParticleSystems[key].Add(system);
				}
			}
		}
	}

	if(mParticleSystems.Contains(type))
		if(mParticleSystems[type].IsValidIndex(num))
			return mParticleSystems[type][num];

	return nullptr;
}