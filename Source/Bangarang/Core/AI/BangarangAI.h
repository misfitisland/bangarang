// Peter Pan's got kids? He can't fly, fight or crow. If any of you says this here scug ain't Peter Pan...cross the line.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"

#include "BangarangAI.generated.h"

class UBGI;

/**
 * 
 */
UCLASS()
class BANGARANG_API ABangarangAI : public AAIController
{
	GENERATED_BODY()
	
public:
							ABangarangAI();

	virtual void			BeginPlay() override;

	UFUNCTION(BlueprintPure)
	UBGI*					GetBangarangAnimInstance();
	UFUNCTION(BlueprintPure)
	EDrawDebugTrace::Type	GetDrawDebugType();
	
private:
	UBGI*			mBGI;
};

inline UBGI* ABangarangAI::GetBangarangAnimInstance() {return mBGI;}
