// Peter Pan's got kids? He can't fly, fight or crow. If any of you says this here scug ain't Peter Pan...cross the line.

#include "BangarangAI.h"

#include "Core/BGI.h"

ABangarangAI::ABangarangAI()
	: AAIController(), mBGI(nullptr)
{
}

void
ABangarangAI::BeginPlay()
{
	Super::BeginPlay();

	if(UBGI* bgi = Cast<UBGI>(GetGameInstance()))
		mBGI = bgi;
}

EDrawDebugTrace::Type
ABangarangAI::GetDrawDebugType()
{
	if(mBGI && mBGI->AIDebug)
		return EDrawDebugTrace::ForDuration;
	
	return EDrawDebugTrace::None;
}