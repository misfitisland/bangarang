// // Copyright 2017-2018 JC Ricks All Rights Reserved.

#include "Utility.h"
#include "Characters/BangarangCharacter.h"

const FString Utility::StrUnknown("Bed, Bath, And Beyond");

FString
Utility::ActionTypeString(const ActionType& type)
{
	switch(type)
	{
		case ActionType::AT_Attack:
			return "Attack";
		case ActionType::AT_ChainedAtk:
			return "ChainedAttack";
		case ActionType::AT_Jump:
			return "Jump";
		case ActionType::AT_Move:
			return "Move";
		case ActionType::AT_Sprint:
			return "Sprint";
		default:
			return StrUnknown;
	};
}

FString
Utility::AttackTypeString(const AttackType& type)
{
	switch(type)
	{
		case AttackType::ATK_Light:
			return "Light";
		case AttackType::ATK_Heavy:
			return "Heavy";
		default:
			return StrUnknown;
	};
}

FString	
Utility::CharacterStateString(const CharacterState& st)
{
	switch(st)
	{
		case CharacterState::CS_Movement:
			return "Movement";
		case CharacterState::CS_Attack:
			return "Attack";
		case CharacterState::CS_ChainedAttack:
			return "ChainedAttack";
		default:
			return "UnknownState";
	};
}

FString
Utility::WeaponCategoryString(const WeaponCategory& category)
{
	switch(category)
	{
		case WeaponCategory::WC_Fists:
			return "Fists";
		case WeaponCategory::WC_LongSword:
			return "LongSword";
		default:
			return StrUnknown;
	};
}

FString
Utility::EffectTypeString(const EffectType& type)
{
	switch(type)
	{
	case EffectType::ET_SwordSlash:
			return "SwordSlash";
	case EffectType::ET_SmokeHit:
			return "SmokeHit";
	case EffectType::ET_SparkHit:
			return "SparkHit";
	default:
		return StrUnknown;
	};
}

AttackType
Utility::StringToAttackType(const FString& str)
{
	if(str.StartsWith("Light"))
		return AttackType::ATK_Light;
	else if(str.StartsWith("Heavy"))
		return AttackType::ATK_Heavy;
	
	return AttackType::ATK_Invalid;
}

WeaponCategory
Utility::StringToWeaponCategory(const FString& str)
{
	if(str.StartsWith("Fists"))
		return WeaponCategory::WC_Fists;
	else if(str.StartsWith("LongSword"))
		return WeaponCategory::WC_LongSword;

	return WeaponCategory::WC_Invalid;
}

EffectType
Utility::StringToEffectType(const FString& str)
{
	if(str.StartsWith("SwordSlash"))
		return EffectType::ET_SwordSlash;
	else if(str.StartsWith("SmokeHit"))
		return EffectType::ET_SmokeHit;
	else if(str.StartsWith("SparkHit"))
		return EffectType::ET_SparkHit;

	return EffectType::ET_Invalid;
}