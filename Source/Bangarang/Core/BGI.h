// Peter Pan's got kids? He can't fly, fight or crow. If any of you says this here scug ain't Peter Pan...cross the line.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"

#include "Utility.h"

#include "BGI.generated.h"

class UAnimationAsset;
class UAssetLibrary;
class UMJAssetLibrary;
class Utility;

class AssetHelper
{
public:
	AssetHelper();

	TSubclassOf<AActor>			GetCharacter(const FString&, bool = false);
	TArray<UAnimationAsset*>	GetCharacterAnimations(const FString&, bool = false);
	TSubclassOf<AActor>			GetWeapon(const FString&, bool = false);
	UParticleSystem*			GetParticleSystem(EffectType, int = 0, bool = false);

	UMJAssetLibrary*	Lib();
	void				SetLib(UMJAssetLibrary*);

private:
	TMap<EffectType, TArray<UParticleSystem*> > mParticleSystems;
	
	UMJAssetLibrary*	mLib;
};

inline UMJAssetLibrary* AssetHelper::Lib() {return mLib;}
inline void AssetHelper::SetLib(UMJAssetLibrary* lib) {mLib = lib;}

/**
 * 
 */
UCLASS(config=Game, transient, BlueprintType, Blueprintable)
class BANGARANG_API UBGI : public UGameInstance
{
	GENERATED_BODY()
	
public:
	UBGI();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Utility|Debug")
	bool			GlobalDebug;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Utility|Debug")
	bool			AIDebug;
 
    void            RequestGameExit(bool);
	AssetHelper&	AHelper();

private:
	UPROPERTY()
	UMJAssetLibrary* mAL;
	
	AssetHelper     mAssetHelper;
};

inline AssetHelper& UBGI::AHelper() {return mAssetHelper;}