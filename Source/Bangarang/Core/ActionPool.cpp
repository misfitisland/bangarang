// // Copyright 2017-2018 JC Ricks All Rights Reserved.

#include "ActionPool.h"

// *** ACTION ***
FAction::FAction()
	: ActType(ActionType::AT_Invalid), IsFresh(true), IsBeingWorkedOn(false)
{
}

FAction::FAction(ActionType ty, TArray<FVariant> da)
	: ActType(ty), Data(da), IsFresh(true), IsBeingWorkedOn(false)
{
}

bool
operator==(const FAction& a, const FAction& b)
{
	if(a.ActType == b.ActType)
	{
		int oCount = b.Data.Num();
		int uCount = a.Data.Num();
		if(oCount == uCount)
		{
			for(int i = 0; i < uCount; ++i)
			{
				if(b.Data[i] != a.Data[i])
					return false;
			}
			
			return true;
		}
	}

	return false;
}

bool
FAction::operator!=(const FAction& other) const
{
	return !(*this == other);
}

// *** FRAME ***
ActionFrame::ActionFrame(TArray<FAction> actions)
	: mFrameActions(actions)
{
}

TArray<FAction*> 
ActionFrame::OfType(ActionType find)
{
	TArray<FAction*> ret;

	auto iter = mFrameActions.CreateIterator();
	auto end = mFrameActions.CreateIterator();
	end.SetToEnd();

	while(iter != end)
	{
		if((*iter).ActType == find)
			ret.Add(&(*iter));

		++iter;
	}

	return ret;
}

void
ActionFrame::Remove(FAction* rem)
{
	int index = mFrameActions.IndexOfByKey(*rem);
	mFrameActions.RemoveAt(index);
}

void
ActionFrame::Remove(TArray<FAction*> rems)
{
	while(rems.Num())
	{
		FAction* rem = rems.Pop();
		Remove(rem);
	}
}

// *** EVALUATOR ***
ActionPool::ActionPool()
	: mFrameComplete(true)
{
}

void
ActionPool::CurrentFrameComplete()
{
	if(mFrames.Num())
		mFrames.Pop();

	mFrameComplete = true;
}

void
ActionPool::AddAction(const FAction& act)
{
	// Create a new frame if we're in a new one
	if(!mFrameComplete)
	{
		CurrentFrame()->Actions().Push(act);
	}
	else
	{
		mFrames.Push(ActionFrame({act}));
		mFrameComplete = false;
	}
}

ActionFrame* 
ActionPool::CurrentFrame() 
{
	if(mFrames.Num()) 
		return &(mFrames.Top());
	else
		return nullptr;
}