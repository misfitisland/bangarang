// // Copyright 2017-2018 JC Ricks All Rights Reserved.

#pragma once

#include "Props/Inventory/Weapons/Weapon.h"

#include "CoreMinimal.h"
#include "Variant.h"

#include "Utility.generated.h"

enum class CharacterState  : uint8;

UENUM(BlueprintType)
enum class Direction : uint8
{
	D_Forward UMETA(DisplayName="Forward"),
	D_Back UMETA(DisplayName="Back"),
	D_Left UMETA(DisplayName="Left"),
	D_Right UMETA(DisplayName="Right"),
	D_Up UMETA(DisplayName="Up"),
	D_Down UMETA(DisplayName="Down")
};

UENUM(BlueprintType)
enum class ActionType : uint8
{
	AT_Move UMETA(DisplayName="Move"),
	AT_Jump UMETA(DisplayName="Jump"),
	AT_Sprint UMETA(DisplayName="Sprint"),
	AT_Attack UMETA(DisplayName="Attack"),
	AT_ChainedAtk UMETA(DisplayName="Chained Attack"),
	AT_Invalid UMETA(DisplayName="Invalid")
};

UENUM(BlueprintType)
enum class MovementType : uint8
{
	MT_MoveForwardV UMETA(DisplayName="Forward Vector"),
	MT_MoveLateralV UMETA(DisplayName="Lateral Vector"),
	MT_Pitch UMETA(DisplayName="Pitch"),
	MT_PitchRate UMETA(DisplayName="Pitch At Rate"),
	MT_Yaw UMETA(DisplayName="Yaw"),
	MT_YawRate UMETA(DisplayName="Yaw At Rate")
};

UENUM(BlueprintType)
enum class AttackType : uint8
{
	ATK_Light UMETA(DisplayName="Light"),
	ATK_Heavy UMETA(DisplayName="Heavy"),
	ATK_Invalid UMETA(DisplayName="Invalid")
};

UENUM(BlueprintType)
enum class EffectType : uint8
{
	ET_SwordSlash UMETA(DisplayName="Sword Slash"),
	ET_SmokeHit UMETA(DisplayName="Smoke Puffs"),
	ET_SparkHit UMETA(DisplayName="Sparking Hit"),
	ET_Invalid UMETA(DisplayName="Invalid")
};

/** Variant Specializations **/
template<> struct TVariantTraits<Direction>
{
	static EVariantTypes GetType() { return EVariantTypes::Custom; }
};

template<> struct TVariantTraits<ActionType>
{
	static EVariantTypes GetType() { return EVariantTypes::Custom; }
};

template<> struct TVariantTraits<MovementType>
{
	static EVariantTypes GetType() { return EVariantTypes::Custom; }
};

template<> struct TVariantTraits<AttackType>
{
	static EVariantTypes GetType() { return EVariantTypes::Custom; }
};

/**
 * Unused rn
 */
class BANGARANG_API Utility
{
public:
	/** String-ify Functions **/
	static FString ActionTypeString(const ActionType&);
	static FString AttackTypeString(const AttackType&);
	static FString CharacterStateString(const CharacterState&);
	static FString WeaponCategoryString(const WeaponCategory&);
	static FString EffectTypeString(const EffectType&);

	/** De-String-ify Functions (Use sparingly, pref only at load) **/
	static AttackType		StringToAttackType(const FString&);
	static WeaponCategory	StringToWeaponCategory(const FString&);
	static EffectType		StringToEffectType(const FString&);

	static const FString StrUnknown;
};
