// Peter Pan's got kids? He can't fly, fight or crow. If any of you says this here scug ain't Peter Pan...cross the line.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "BangarangPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class BANGARANG_API ABangarangPlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	ABangarangPlayerState();
	
	bool bInitNeeded;
	float HP;
	float Mana;
	float Stamina;
};
