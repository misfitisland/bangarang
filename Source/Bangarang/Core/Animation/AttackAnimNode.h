// Peter Pan's got kids? He can't fly, fight or crow. If any of you says this here scug ain't Peter Pan...cross the line.

#pragma once

#include "Core/Utility.h"

#include "CoreMinimal.h"
#include "Animation/AnimNodeBase.h"

#include "AttackAnimNode.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct BANGARANG_API FAttackAnimNode : public FAnimNode_Base
{ 
	GENERATED_USTRUCT_BODY()
	
public:
						FAttackAnimNode();
	virtual				~FAttackAnimNode() = default;

	virtual void 		Initialize_AnyThread(const FAnimationInitializeContext&) 	override;
	virtual void		Update_AnyThread(const FAnimationUpdateContext&)			override;
	virtual void		Evaluate_AnyThread(FPoseContext&)							override;
	virtual void		GatherDebugData(FNodeDebugData&)							override;

	/* Game Thread PreUpdate */
	//virtual bool		HasPreUpdate() const final { return true; }
	//virtual void		PreUpdate(const UAnimInstance* InAnimInstance) final;

protected:
	UPROPERTY()
	UAnimSequence*		CurrentEntry;
		
	// Marker tick record for this play through
	FMarkerTickRecord MarkerTickRecord;

	float	PreviousTimeAccumulator;
	float	InternalTimeAccumulator;
};