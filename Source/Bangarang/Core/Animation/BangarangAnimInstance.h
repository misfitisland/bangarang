// Peter Pan's got kids? He can't fly, fight or crow. If any of you says this here scug ain't Peter Pan...cross the line.

#pragma once

#include "Characters/BangarangCharacter.h"
#include "Core/ActionPool.h"

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "Animation/AnimInstanceProxy.h"

#include "BangarangAnimInstance.generated.h"

/** Proxy object passed around during animation tree update in lieu of a UAnimInstance */
USTRUCT()
struct BANGARANG_API FBangarangAnimInstanceProxy : public FAnimInstanceProxy
{
	GENERATED_BODY()

public:
					FBangarangAnimInstanceProxy(UAnimInstance* Instance);
					FBangarangAnimInstanceProxy() = default;
	virtual			~FBangarangAnimInstanceProxy() = default;

	bool			AttackStateJustChanged;
	int				CurrentStateLoopCount;
	float			CurrentAnimTime;
	float			CurrentAnimTimeFraction;
	FAction			NextAction;
};

/**
 * 
 */
UCLASS()
class BANGARANG_API UBangarangAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
	typedef FBangarangAnimInstanceProxy ProxyType;

public:
				UBangarangAnimInstance();

	/** Cool Kid C++ Custom Behavior Update 
	*  Does stuff like expose the current animation state for usage elsewhere
	*/
	UFUNCTION(BlueprintCallable, Category="Update|C++ Class")
	void			BlueprintTick();

	/**!!! Blueprint Notification Work Functions !!!**/

	/** Checks queued action against current action
	* Breaks animation if not queued up, or combos if diff type of attack
	*/
	UFUNCTION(BlueprintCallable, Category="Notify|Attack|C++ Class")
	bool			AttackCheck();

	UFUNCTION(BlueprintCallable, Category="Notify|Attack|C++ Class")
	void			OpenAttackWindow();

	UFUNCTION(BlueprintCallable, Category="Notify|Attack|C++ Class")
	void			CloseAttackWindow();

	UFUNCTION(BlueprintCallable, Category="Notify|Attack|C++ Class")
	bool			IsAttackInputWindowOpen();

	UFUNCTION(BlueprintCallable, Category="Notify|States|C++ Class")
	void			OnEnterMovementState();

	UFUNCTION(BlueprintCallable, Category="Notify|States|C++ Class")
	void			OnLeaveMovementState();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Animation)
	CharacterState	BreakToState;

	/** !!! END BP WORK !!! **/

protected:
	virtual FAnimInstanceProxy* CreateAnimInstanceProxy() override;

private:
	virtual void PreUpdateAnimation(float) final;

	void		CheckAndFillRequiredMembers();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	int			CurrentAnimTimeRemaining;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	int			CurrentAnimState;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	float		TimeRemainingFraction;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	int	    	CurrentAnimLoopCount;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	bool		IsInAir;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	float		Speed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	ABangarangCharacter*	OwningCharacter;	
	
	bool					mIsAttackInputWindowOpen;
	bool					mJustChangedStates;
	int						mPreviousAnimState;
	int						mStateMachineIndex;
	FAnimNode_StateMachine*	mBaseStateMachine;
};