// Peter Pan's got kids? He can't fly, fight or crow. If any of you says this here scug ain't Peter Pan...cross the line.

#include "BangarangAnimInstance.h"

#include "Animation/AnimNode_StateMachine.h"


FBangarangAnimInstanceProxy::FBangarangAnimInstanceProxy(UAnimInstance* Instance)
	: FAnimInstanceProxy(Instance), AttackStateJustChanged(false), CurrentStateLoopCount(0), CurrentAnimTime(0.f),
	  CurrentAnimTimeFraction(1.f)
{
}

UBangarangAnimInstance::UBangarangAnimInstance()
	: UAnimInstance(), BreakToState(CharacterState::CS_Invalid), CurrentAnimTimeRemaining(0), 
	  TimeRemainingFraction(0.0f), CurrentAnimLoopCount(0), OwningCharacter(nullptr), mIsAttackInputWindowOpen(true),
	  mJustChangedStates(false), mPreviousAnimState(-1), mStateMachineIndex(-1), mBaseStateMachine(nullptr)
{
	// All my actors start in movement state
	CurrentAnimState	= static_cast<int>(CharacterState::CS_Movement);
	mPreviousAnimState	= CurrentAnimState;
}

FAnimInstanceProxy*
UBangarangAnimInstance::CreateAnimInstanceProxy()
{
	return new FBangarangAnimInstanceProxy(this);
}

void 
UBangarangAnimInstance::PreUpdateAnimation(float DeltaSeconds)
{
	CheckAndFillRequiredMembers();

	UAnimInstance::PreUpdateAnimation(DeltaSeconds);
}

void
UBangarangAnimInstance::BlueprintTick()
{
	if(OwningCharacter)
	{
		// DEBUG OUTPUT STUFF IF YOU WANT
		/*if(!GWorld->IsEditorWorld() || GWorld->IsPlayInEditor())
			if(!IsAttackInputWindowOpen())
				UE_LOG(LogTemp, Log, TEXT("Window Closed!"));	*/

		// Update Blueprint Visible Data
		IsInAir		= OwningCharacter->IsInAir();
		Speed		= OwningCharacter->GetSpeed();

		// Update our state
		if(mBaseStateMachine)
		{
			CurrentAnimState		 = mBaseStateMachine->GetCurrentState();
			mJustChangedStates  	 = CurrentAnimState != mPreviousAnimState;
		}

		FBangarangAnimInstanceProxy& Proxy	= GetProxyOnGameThread<FBangarangAnimInstanceProxy>();
		CharacterState st = static_cast<CharacterState>(CurrentAnimState);

		// Fresh goods only
		FAction& next = OwningCharacter->NextAction;
		if(next.IsFresh && !next.IsBeingWorkedOn)
		{
			switch(st)
			{
				case CharacterState::CS_Movement:
				{
					// Break our animation state
					if(next.ActType == ActionType::AT_Attack)
					{
						BreakToState = CharacterState::CS_Attack;
						Proxy.NextAction = OwningCharacter->NextAction;
						next.IsBeingWorkedOn = true;
					}
					else if(next.ActType == ActionType::AT_ChainedAtk)
					{
						BreakToState = CharacterState::CS_ChainedAttack;
						Proxy.NextAction = OwningCharacter->NextAction;
						next.IsBeingWorkedOn = true;
					}
				}
				break;

				case CharacterState::CS_Attack:
				case CharacterState::CS_ChainedAttack:
				{
					// Update our proxy so our animations can act on this
					Proxy.NextAction = OwningCharacter->NextAction;
					next.IsBeingWorkedOn = true;
				}
				break;

				// The hell state are we in?
				default:
					return;
			}
		}

		// Movement's time is reported the ole normal way
		// Actions are reported by me using my custom nodes and custom proxy if needed (Eg: Attacks)
		if(static_cast<CharacterState>(CurrentAnimState) == CharacterState::CS_Movement)
		{
			CurrentAnimTimeRemaining = GetRelevantAnimTimeRemaining(mStateMachineIndex, CurrentAnimState);
			TimeRemainingFraction    = GetRelevantAnimTimeRemainingFraction(mStateMachineIndex, CurrentAnimState);
		}
		else
		{
			CurrentAnimTimeRemaining	= Proxy.CurrentAnimTime;
			TimeRemainingFraction		= Proxy.CurrentAnimTimeFraction;
		}

		// handle if we've changed states since last frame
		if(mJustChangedStates)
		{
			OwningCharacter->mCurrentState = CurrentAnimState;
			mPreviousAnimState = CurrentAnimState;

			BreakToState			= CharacterState::CS_Invalid;
			CurrentAnimLoopCount	= 0;
			mJustChangedStates		= false;
		}
		else if(CurrentAnimLoopCount != Proxy.CurrentStateLoopCount)
		{
			// Update loop count from proxy
			CurrentAnimLoopCount = Proxy.CurrentStateLoopCount;
		}
	}
}

bool
UBangarangAnimInstance::AttackCheck()
{
	/*UE_LOG(LogTemp, Warning, TEXT("Attack Check!"));

	FString curMsg = FString("Current : ").Append(Utility::ActionTypeString(OwningCharacter->CurrentAction.ActType));
	FString futMsg = FString("Future  : ").Append(Utility::ActionTypeString(OwningCharacter->NextAction.ActType));*/

	bool changed = false;

	ActionType fut = OwningCharacter->NextAction.ActType;
	if(fut == ActionType::AT_Invalid)
	{
		BreakToState = CharacterState::CS_Movement;
		changed = true;
	}
	else if(fut == ActionType::AT_Attack)
	{
		BreakToState = CharacterState::CS_Attack;
		changed = true;
	}
	else if(fut == ActionType::AT_ChainedAtk)
	{
		BreakToState = CharacterState::CS_ChainedAttack;
		changed = true;
	}

	//UE_LOG(LogTemp, Log, TEXT("%s"), *curMsg);
	//UE_LOG(LogTemp, Log, TEXT("%s"), *futMsg);

	//// Action type was the same as last or movement, just save it as current like usual
	//UE_LOG(LogTemp, Log, TEXT("Setting Current: AttackCheck"));
	OwningCharacter->CurrentAction = OwningCharacter->NextAction;
	OwningCharacter->NextAction = FAction();
	OwningCharacter->NextAction.IsFresh = false;

	CloseAttackWindow();

	return changed;
}

void 
UBangarangAnimInstance::OpenAttackWindow() 
{
	//UE_LOG(LogTemp, Log, TEXT("OpenAttackWindow"));
	mIsAttackInputWindowOpen = true;
	OwningCharacter->bHasDoneDamageThisSwing = false;
}

void
UBangarangAnimInstance::CloseAttackWindow()
{
	//UE_LOG(LogTemp, Log, TEXT("CloseAttackWindow"));
	mIsAttackInputWindowOpen = false;
}

bool
UBangarangAnimInstance::IsAttackInputWindowOpen()
{
	return mIsAttackInputWindowOpen;
}

void
UBangarangAnimInstance::OnEnterMovementState()
{
	OpenAttackWindow();
}

void
UBangarangAnimInstance::OnLeaveMovementState()
{
	OwningCharacter->CurrentAction = OwningCharacter->NextAction;
	OwningCharacter->NextAction = FAction();
	OwningCharacter->NextAction.IsFresh = false;
	CloseAttackWindow();
}

void
UBangarangAnimInstance::CheckAndFillRequiredMembers()
{
	if(!OwningCharacter)
	{
		OwningCharacter = Cast<ABangarangCharacter>(TryGetPawnOwner());

		if(!OwningCharacter)
		{
			if(!GWorld->IsEditorWorld())
			{
				UE_LOG(LogAnimation, Error, TEXT("Owning Character Isn't Resolved..."));
			}

			return;
		}
	}

	if(!mBaseStateMachine)
	{
		mBaseStateMachine  = GetStateMachineInstanceFromName(FName("AnimStates"));
		mStateMachineIndex = GetStateMachineIndex(FName("AnimStates"));
		mPreviousAnimState = mBaseStateMachine->GetCurrentState();

		if(!mBaseStateMachine)
		{
			UE_LOG(LogAnimation, VeryVerbose,
				TEXT("BangarangAnimInstance : Animation State machine couldn't be resolved."));
			return;
		}
	}
}