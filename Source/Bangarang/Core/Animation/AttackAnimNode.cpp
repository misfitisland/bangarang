// Peter Pan's got kids? He can't fly, fight or crow. If any of you says this here scug ain't Peter Pan...cross the line.

#include "AttackAnimNode.h"

#include "Characters/BangarangCharacter.h"
#include "Core/Animation/BangarangAnimInstance.h"

#include "Engine.h"
#include "Runtime/Engine/Public/Animation/AnimInstanceProxy.h"

FAttackAnimNode::FAttackAnimNode()
	: FAnimNode_Base(), CurrentEntry(nullptr), PreviousTimeAccumulator(0.f), InternalTimeAccumulator(0.f)
{
}

void
FAttackAnimNode::Initialize_AnyThread(const FAnimationInitializeContext& Context)
{
	FAnimNode_Base::Initialize_AnyThread(Context);
	EvaluateGraphExposedInputs.Execute(Context);

	ABangarangCharacter* owningChar = 
					Cast<ABangarangCharacter>(Context.AnimInstanceProxy->GetSkelMeshComponent()->GetOwner());
	
	FBangarangAnimInstanceProxy* proxy = 
			static_cast<FBangarangAnimInstanceProxy*>(Context.AnimInstanceProxy);

	FAction& Next = proxy->NextAction;

	// I don't want to check if we have a thing loaded every time
	AttackType at			= Next.Data[0].GetValue<AttackType>();
	WeaponCategory wc		= Next.Data[1].GetValue<WeaponCategory>();
	Direction dir			= Next.Data[2].GetValue<Direction>();

	CurrentEntry		= Cast<UAnimSequence>(owningChar->GetAttackAnim(at, wc, dir));
	InternalTimeAccumulator = 0.f;
	PreviousTimeAccumulator = 0.f;
			
	proxy->CurrentAnimTime			= InternalTimeAccumulator;
	proxy->CurrentAnimTimeFraction	= 1.0f;
	proxy->CurrentStateLoopCount	= 0;
	proxy->AttackStateJustChanged   = false;
}

void
FAttackAnimNode::Update_AnyThread(const FAnimationUpdateContext & Context)
{
	EvaluateGraphExposedInputs.Execute(Context);

	if(!CurrentEntry)
	{
		// We don't have any entries, play data will be invalid - early out
		return;
	}

	FBangarangAnimInstanceProxy* proxy = 
			static_cast<FBangarangAnimInstanceProxy*>(Context.AnimInstanceProxy);

	// Update various stats like playtime
	if(proxy)
	{
		if(UAnimSequence* seq = CurrentEntry)//Entries[CurrentEntry].Sequence)
		{
			float TimeRemaining = seq->SequenceLength - InternalTimeAccumulator;
			

			proxy->CurrentAnimTime = InternalTimeAccumulator;
			proxy->CurrentAnimTimeFraction = (seq->SequenceLength - InternalTimeAccumulator) / seq->SequenceLength;

			// Looped
			if(InternalTimeAccumulator < PreviousTimeAccumulator)
				proxy->CurrentStateLoopCount++;
				
			// Cache previous time to detect loops
			PreviousTimeAccumulator = InternalTimeAccumulator;

			// tick record update
			FAnimGroupInstance* SyncGroup;
			FAnimTickRecord& TickRecord = proxy->CreateUninitializedTickRecord(INDEX_NONE, SyncGroup);
			proxy->MakeSequenceTickRecord(TickRecord, seq, true, 1.f, 1.f, InternalTimeAccumulator, MarkerTickRecord);
		}
	}
}

void
FAttackAnimNode::Evaluate_AnyThread(FPoseContext& Output)
{
	if(CurrentEntry)
	{
		if(UAnimSequence* seq = CurrentEntry)
			seq->GetAnimationPose(Output.Pose, Output.Curve, FAnimExtractContext(InternalTimeAccumulator, Output.AnimInstanceProxy->ShouldExtractRootMotion()));
		else
			Output.ResetToRefPose();
	}
	else
	{
		Output.ResetToRefPose();
	}
}

void
FAttackAnimNode::GatherDebugData(FNodeDebugData& dd)
{
	FString DebugLine = dd.GetNodeName(this);
	dd.AddDebugItem(DebugLine, true);
}

//void
//FAttackAnimNode::PreUpdate(const UAnimInstance* InAnimInstance)
//{
//	//const UBangarangAnimInstance* bluh = static_cast<const UBangarangAnimInstance*>(InAnimInstance);
//	//UBangarangAnimInstance* cBAI = const_cast<UBangarangAnimInstance*>(bluh);
//	//if(cBAI)
//	//{
//
//	//}
//}