// // Copyright 2017-2018 JC Ricks All Rights Reserved.

#pragma once

#include "Utility.h"

#include "CoreMinimal.h"
#include "Variant.h"

#include "ActionPool.generated.h"

/**
 * Action 
 *	Is Some Action Interpreted From Some Source (Eg: Player Input or AI)
 *	Are Composed of a main Type and a TArray<FVariant> that holds any data
 * 
 *  Generally Action Data Ordering Follows These Rules
 *  1 - If a type doesn't have one of these data types, then it's not included
		Eg) AT_Jump doesn't need a subtype or any other data so it includes nothing
 *	2 - Ordering SHOULD be in this order if an action has it
		- Action Sub-Type Eg: AttackType::AT_Light
		- Action Direction Eg: Direction:D_Up
		- Any Number Of Values Relevant to the Action Eg: Yaw Rate, objects, bool, int, etc
 *
 */
USTRUCT(BlueprintType, Blueprintable)
struct BANGARANG_API FAction
{
	GENERATED_USTRUCT_BODY()

						FAction();
						FAction(ActionType, TArray<FVariant> = TArray<FVariant>());
	virtual				~FAction() = default;
	bool				operator!=(const FAction&) const;

	// This style of ==operator definition, defined outside the class, is needed by
	// TArrays to make comparisons I guess for removal and such...
	friend bool			operator==(const FAction&, const FAction&);

	UPROPERTY(Category = "Action: Type", EditAnywhere, BlueprintReadWrite, meta=(PinShownByDefault))
	ActionType			ActType;
	
	TArray<FVariant>	Data;
	bool				IsFresh;
	bool				IsBeingWorkedOn;
};

struct BANGARANG_API ActionFrame
{
	ActionFrame() = default;
	ActionFrame(TArray<FAction>);
	virtual ~ActionFrame() = default;

	int32			Count();
	TArray<FAction>&	Actions();

	TArray<FAction*> OfType(ActionType);
	
	void			Remove(FAction*);
	void			Remove(TArray<FAction*>);

private:
	TArray<FAction>	mFrameActions;
};

inline int32 ActionFrame::Count() {return mFrameActions.Num();}
inline TArray<FAction>& ActionFrame::Actions() {return mFrameActions;}

/**
 * 
 */
class BANGARANG_API ActionPool
{
public:
						ActionPool();
	virtual				~ActionPool() = default;

	void				CurrentFrameComplete();

	void				AddAction(const FAction&);
	ActionFrame*		CurrentFrame();

private:
	bool				mFrameComplete;
	TArray<ActionFrame>	mFrames;
};