// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.IO;

public class Bangarang : ModuleRules
{
	public Bangarang(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange    (new string[] { "Core", "CoreUObject", "Engine", "AnimGraphRuntime", "MJAssetLibrary"});
        PrivateDependencyModuleNames.AddRange   (new string[] { "InputCore", "AnimGraphRuntime"});

        PublicIncludePaths.AddRange(new string[] {});
        PublicIncludePaths.Add(Path.GetFullPath(Path.Combine(EngineDirectory, "Plugins/Marketplace/MJAssetLibrary/Source/MJAssetLibrary/Public")));
        PublicIncludePaths.Add(Path.GetFullPath(Path.Combine(EngineDirectory, "Plugins/Marketplace/MJAssetLibrary/Source/MJAssetLibrary/Classes")));
    }
}
