// Peter Pan's got kids? He can't fly, fight or crow. If any of you says this here scug ain't Peter Pan...cross the line.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InventoryItem.generated.h"

UENUM(BlueprintType)
enum class ItemCategory : uint8
{
	IC_Consumable UMETA(DisplayName="Consumable"),
	IC_Weapon UMETA(DisplayName="Weapon"),
	IC_Armor UMETA(DisplayName="Armor"),
	IC_Invalid UMETA(DisplayName="Invalid")
};

USTRUCT(BlueprintType, Blueprintable)
struct BANGARANG_API FItemDeets
{
	GENERATED_BODY()

public:
	float			Weight;
	bool			Equippable;
	FName			Name;
};

UCLASS(BlueprintType, Blueprintable, abstract)
class BANGARANG_API AInventoryItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInventoryItem();

	FItemDeets&		Deets();

	// Subclasses should manage this
	UFUNCTION(BlueprintCallable)
	virtual ItemCategory Category() PURE_VIRTUAL(AInventoryItem::Category, return ItemCategory::IC_Invalid;);

private:
	FItemDeets		mDeets;
};

inline FItemDeets&	AInventoryItem::Deets() {return mDeets;}