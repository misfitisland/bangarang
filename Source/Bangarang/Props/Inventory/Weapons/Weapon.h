// Peter Pan's got kids? He can't fly, fight or crow. If any of you says this here scug ain't Peter Pan...cross the line.

#pragma once

#include "CoreMinimal.h"
#include "Variant.h"

#include "Props/Inventory/InventoryItem.h"

#include "Weapon.generated.h"

UENUM(BlueprintType)
enum class WeaponCategory : uint8
{
	WC_Fists UMETA(DisplayName="Fists"),
	WC_LongSword UMETA(DisplayName="Long Sword"),
	WC_Invalid UMETA(DisplayName="Invalid")
};

/*
* UClass weapon interface for muh stuff
* mainly using it for shared properties rn
*/
UCLASS(abstract)
class BANGARANG_API AWeapon : public AInventoryItem
{
	GENERATED_BODY()
	
public:	
	AWeapon();	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
	FString WeaponName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
	float   BaseDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
	WeaponCategory WeaponCategory;

	// Subclasses should manage this
	UFUNCTION(BlueprintCallable)
	virtual float FinalDamage() PURE_VIRTUAL(AWeapon::FinalDamage, return 0.f;);

	virtual ItemCategory Category() final;

protected:
	UPROPERTY()
	class AHuman*	CurrentlyEquippedBy;
	UPROPERTY()
	class UBGI*		mBGI;
};

inline ItemCategory AWeapon::Category() {return ItemCategory::IC_Weapon;}

/** Variant Specializations **/
template<> struct TVariantTraits<WeaponCategory>
{
	static EVariantTypes GetType() { return EVariantTypes::Custom; }
};