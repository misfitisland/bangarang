// Peter Pan's got kids? He can't fly, fight or crow. If any of you says this here scug ain't Peter Pan...cross the line.

#include "Melee.h"
#include "Characters/Human.h"
#include "Core/BGI.h"

#include "Runtime/Engine/Classes/Components/SkeletalMeshComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

#include "DrawDebugHelpers.h"

// Sets default values
AMelee::AMelee()
	: AWeapon()
{
	PrimaryActorTick.bCanEverTick = false;

	FScriptDelegate over;
	over.BindUFunction(this, TEXT("OnOverlap"));

	OnActorBeginOverlap.Add(over);
}

float
AMelee::FinalDamage()
{
	return BaseDamage;
}

// Called when the game starts or when spawned
void 
AMelee::BeginPlay()
{
	Super::BeginPlay();

	AActor* owner = GetOwner();
	if(!owner)
		return;

	CurrentlyEquippedBy = Cast<AHuman>(owner);
	mBGI = Cast<UBGI>(GetGameInstance());
}

void
AMelee::Tick(float deltaTime)
{
	if(!GWorld->IsEditorWorld() || GWorld->IsPlayInEditor())
	{
		UE_LOG(LogTemp, Log, TEXT("Weapon X Vel : %s"), *FString::SanitizeFloat(this->GetVelocity().X));
	}
}

void 
AMelee::OnOverlap(AActor* OverlappedActor, AActor* OtherActor)
{  
	// This is used for attacks and should never be called if we have no parent actor
	// (for now) we can harness this branch in logic later to pick up weapons
	if(!CurrentlyEquippedBy)
		return;

    // Other Actor is the actor that triggered the event. Check that is not ourself.  Or the rents.
    if ((OtherActor != nullptr ) && 
		(OtherActor != CurrentlyEquippedBy) &&
		(OtherActor != this))  
    {  
		if(ABangarangCharacter* ch = Cast<ABangarangCharacter>(OverlappedActor))
		{
			UE_LOG(LogTemp, Log, TEXT("Overlapped: %s"), *ch->GetCharacterName());
		}

		if(CurrentlyEquippedBy->CanCauseDamage())
		{
			FCollisionQueryParams TraceParams = FCollisionQueryParams(FName(TEXT("MeleeTrace")), true, this);
			TraceParams.bTraceComplex = true;
			TraceParams.bTraceAsyncScene = true;
			TraceParams.bReturnPhysicalMaterial = false;
 
			//Re-initialize hit info
			FHitResult hitResult(ForceInit);

			FVector exts;
			FVector weaponOrigin;
			FVector actorLoc = GetActorLocation();
			FVector rightVec = GetActorRightVector();

			GetActorBounds(true, weaponOrigin, exts);

			// Base starts litte up from the visual base so we don't hit our owner's hand/mandible/claw
			FVector weaponBase	= actorLoc - (rightVec * exts * 0.2);
			FVector weaponTip 	= weaponOrigin - (rightVec * exts);

			FCollisionShape colShape;
			colShape.ShapeType = ECollisionShape::Sphere;
			
			// Ray-Trace from this weapon's center on its forward vector magnified by 5, see what we hit
			if(GetWorld()->SweepSingleByChannel(hitResult, weaponBase, weaponTip, FQuat::Identity, ECollisionChannel::ECC_Visibility, colShape, TraceParams))
			{
				if(mBGI->GlobalDebug)
					DrawDebugLine(GWorld, weaponBase, weaponTip, FColor::Blue, true);
			}
			
			// Apply the damage
			UGameplayStatics::ApplyPointDamage(OtherActor, FinalDamage(), GetActorUpVector() * -1.0f, hitResult, CurrentlyEquippedBy->GetController(), this, UDamageType::StaticClass());
		}
    }  
}