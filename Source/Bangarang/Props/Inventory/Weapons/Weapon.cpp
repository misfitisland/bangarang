// Peter Pan's got kids? He can't fly, fight or crow. If any of you says this here scug ain't Peter Pan...cross the line.

#include "Weapon.h"


// Sets default values
AWeapon::AWeapon()
	: AInventoryItem(), WeaponName("A Stick"), BaseDamage(0.f), WeaponCategory(WeaponCategory::WC_Invalid),
	  CurrentlyEquippedBy(nullptr), mBGI(nullptr)
{
}