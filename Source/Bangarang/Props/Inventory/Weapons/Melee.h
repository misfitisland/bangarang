// Peter Pan's got kids? He can't fly, fight or crow. If any of you says this here scug ain't Peter Pan...cross the line.

#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"

#include "Melee.generated.h"

UCLASS()
class BANGARANG_API AMelee : public AWeapon
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMelee();

	virtual float FinalDamage() final;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Tick(float deltaTime) final;

private:	
	UFUNCTION()
	void OnOverlap(AActor* OverlappedActor, AActor* OtherActor);
};